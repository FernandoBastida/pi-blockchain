import json
import time
import requests
import psycopg2 as db

class FillDb:

    scheduler = None
    connection = None


    def __init__(self):
        # Inicializar la conexion con la base de datos.
        self.connection = db.connect("user=postgres dbname=price_history")


    def fill_data_base(self, interval = 10):
        while True:
            self.insert_to_db(self.get_bitso_data())
            time.sleep(interval)

    def get_bitso_data(self):
        # Obtener los datos de Bitso.
        data = requests.get("https://api.bitso.com/v3/ticker/")
        # Parsear Json.
        data = json.loads(data.text)["payload"]
        return data


    def insert_to_db(self, data):
        # Crear cursor.
        cursor = self.connection.cursor()
        # Revisar si se tiene una lista de datos o un dato individual
        if(type(data) is list):
            for d in data:
                # Ejecutar insercion.
                insert = self.json2insert_string(d)
                cursor.execute(insert[0], insert[1])
        elif(type(data) is dict):
            # Ejecutar insercion.
            insert = self.json2insert_string(data)
            cursor.execute(insert[0], insert[1])
        else:
            print("Error")
            return False
        # Commit changes.
        cursor.close()
        self.connection.commit()


    def json2insert_string(self, json):
        insert_string = "INSERT INTO bitso (book, volume, vwap, high, low, "
        insert_string = insert_string + "ask, bid, last, created_at)  VALUES "
        insert_string = insert_string + "(%s, %s, %s, %s, %s, %s, %s, %s, %s)"
        insert_values = (json['book'], json['volume'],
                        json['vwap'], json['high'], json['low'], json['ask'],
                        json['bid'], json['last'], json['created_at'])
        return (insert_string, insert_values)


if  __name__  ==  '__main__':
    #  Execute  $sudo  -u  postgres  python  /pathToScript/get_bitso_data.py
    bitso  =  FillDb()
    bitso.fill_data_base(30)
