# For this script to work is necesary to have the 
# default postgres user and password and to execute }
# it as root.

# Get ins psql command line
sudo -u postgres psql << EOF

create database price_history;
\c price_history;
create table bitso(
  book char(7) NOT NULL,
  volume varchar(20) NOT NULL,
  vwap varchar(20) NOT NULL,
  high varchar(20) NOT NULL,
  low varchar(20) NOT NULL,
  ask varchar(20) NOT NULL,
  bid varchar(20) NOT NULL,
  last varchar(20) NOT NULL,
  created_at timestamp
  ); 