import json
import time
import requests
import psycopg2 as db

class fillDB:

  scheduler = None
  connection = None


  def __init__(self):
    # Inicializar la conexion con la base de datos.
    self.connection = db.connect("user=postgres dbname=price_history")


  def fillDataBase(self, interval = 10):
    while True:
      self.insertToDB(self.getBitsoData())
      time.sleep(interval)

  def getBitsoData(self):
    # Obtener los datos de Bitso.
    data = requests.get("https://api.bitso.com/v3/ticker/")
    # Parsear Json.
    data = json.loads(data.text)["payload"]
    return data


  def insertToDB(self, data):
    # Crear cursor.
    cursor = self.connection.cursor()
    # Revisar si se tiene una lista de datos o un dato individual
    if(type(data) is list):
      for d in data:
        # Ejecutar insercion.
        insert = self.json2InsertString(d)
        cursor.execute(insert[0], insert[1])
    elif(type(data) is dict):
      # Ejecutar insercion.
      insert = self.json2InsertString(data)
      cursor.execute(insert[0], insert[1])
    else:
      print("Error")
      return False
    # Commit changes.
    cursor.close()
    self.connection.commit()


  def json2InsertString(self, json):
    insertString = "INSERT INTO bitso (book, volume, vwap, high, low, "
    insertString = insertString + "ask, bid, last, created_at)  VALUES "
    insertString = insertString + "(%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    insertValues = (json['book'], json['volume'], 
      json['vwap'], json['high'], json['low'], json['ask'], 
      json['bid'], json['last'], json['created_at'])
    return (insertString, insertValues)


if __name__ == '__main__':
  # Execute $sudo -u postgres python /pathToScript/getBitsoData.py
  bitso = fillDB()
  bitso.fillDataBase(30)


    