import ccxt
import numpy as np
import pandas as pd
import json
import time
from  os.path import join, abspath, dirname
from functools import reduce

try:
    __file__
except NameError:
    print ('Redefining __file__')
    __file__ = abspath('/home/rene/personal-workspace/exchange-blockchain/scripts/load_information.py')
    print(__file__)

script_path = dirname(abspath(__file__))
print(script_path)

fundingsBitso = pd.read_csv(abspath(join(script_path, '../data/bitso.com-fundings-01172018-1545.csv')))
tradesBitso = pd.read_csv(abspath(join(script_path, '../data/bitso.com-trades-01172018-1545.csv')))
withdrawalsBitso = pd.read_csv(abspath(join(script_path, '../data/bitso.com-withdrawals-01182018-1138.csv')))
historyBitso = pd.read_csv(abspath(join(script_path, '../data/bitsoMXN.csv')), header=None)
fundingsPoloniex = pd.read_csv(abspath(join(script_path, '../data/depositHistory.csv')))
bitcoinMarket = pd.read_csv(abspath(join(script_path, '../data/market-price.csv')), header=None)
tradesPoloniex = pd.read_csv(abspath(join(script_path, '../data/tradeHistory.csv')))

#####################################
# ################################# #
# # Revisión de comisiones en CSV # #
# ################################# #
#####################################

#########
# Bitso #
#########

# Bitso, ventas generales, total es en valor minor: fee + total = value (valor minor)

tradesBitso[tradesBitso['type'] == 'sell']['fee'] + tradesBitso[tradesBitso['type'] == 'sell']['total'] - tradesBitso[tradesBitso['type'] == 'sell']['value']
percentage_sell_fee = tradesBitso[tradesBitso['type'] == 'sell']['fee'] / tradesBitso[tradesBitso['type'] == 'sell']['value']

# Bitso, compras generales, total es un valor major: fee + total = amount (valor major)

tradesBitso[tradesBitso['type'] == 'buy']['fee'] + tradesBitso[tradesBitso['type'] == 'buy']['total'] - tradesBitso[tradesBitso['type'] == 'buy']['amount']

percentage_buy_fee = tradesBitso[tradesBitso['type'] == 'buy']['fee'] / tradesBitso[tradesBitso['type'] == 'buy']['amount']

# Determinación de porcentajes de cobro
tradesBitso['percentageFee'] = 0.
tradesBitso.loc[ tradesBitso['type'] == 'buy', 'percentageFee'] = percentage_buy_fee
tradesBitso.loc[ tradesBitso['type'] == 'sell', 'percentageFee'] = percentage_sell_fee

tradesPoloniex[tradesPoloniex['Type'] == 'Buy']['Fee']

# ----------------------------------------

############
# Poloniex #
############

# Price * Amount = Total
tradesPoloniex['Price'] * tradesPoloniex['Amount'] - tradesPoloniex['Total']

# Base total: Total en la primera moneda menos comisión, es un incremento respecto a los saldos anteriores
# Quote total: Total en la segunda moneda menos comisión, es un incremento respecto a los saldos anteriores

# Amount: cantidad en la primera moneda del mercado
# Total: cantidad en la segunda moneda del mercado

# Los conceptos de taker/maker no son equivalentes a compra/venta, se
# refiere más bien a si la orden se ejecuta al momento (taker) o se
# queda registrada en el sistema (maker) para futuras compras

# Esto afecta las comisiones

tradesPoloniex['percentageFee'] = 0.
tradesPoloniex.loc[ tradesPoloniex['Type'] == 'Sell', 'percentageFee' ] = 1 - tradesPoloniex[tradesPoloniex['Type'] == 'Sell']['Base Total Less Fee'] / tradesPoloniex[tradesPoloniex['Type'] == 'Sell']['Total']
tradesPoloniex.loc[ tradesPoloniex['Type'] == 'Buy', 'percentageFee' ] = 1 - tradesPoloniex[tradesPoloniex['Type'] == 'Buy']['Quote Total Less Fee'] / tradesPoloniex[tradesPoloniex['Type'] == 'Buy']['Amount']


#######################################
# ################################### #
# # Utilización de API keys (Bitso) # #
# ################################### #
#######################################


bitso_jc_file = open(abspath(join(script_path, '../api_keys/bitso_juan_secret.json'))).read()
bitso_jc_json = json.loads(bitso_jc_file)

bitso_jc_api = ccxt.bitso(
    {'apiKey': bitso_jc_json['clave'],
     'secret': bitso_jc_json['secreto'],
     'nonce': ccxt.Exchange.milliseconds})

bitso_jc_api.load_markets()

bitso_file = open(abspath(join(script_path, '../api_keys/bitso_secret.json'))).read()
bitso_json = json.loads(bitso_file)

bitso_api = ccxt.bitso(
    {'apiKey': bitso_json['clave'],
     'secret': bitso_json['secreto'],
     'nonce': ccxt.Exchange.milliseconds})

bitso_api.load_markets()

def has_OHLCV_valuetype(id, valuetype):
    temp = getattr(ccxt, id)().has['fetchOHLCV']
    return temp == valuetype

def has_property_valuetype(id, property, valuetype):
    """
    Function test if an the element `id' of ccxt.exchanges has property equal to valuetype
    """
    temp = getattr(ccxt, id)().has[property]
    return temp == valuetype

# Lista de exchanges que permiten emular OHLCV
emulated_list = list(filter(lambda exchange: has_OHLCV_valuetype(exchange, 'emulated'), ccxt.exchanges ))
# Lista de exchanges que ofrecen OHLCV
available_list = list(filter(lambda exchange: has_OHLCV_valuetype(exchange, True), ccxt.exchanges ))
# Lista de exchanges que no ofreces OHLCV
unavailable_list = list(filter(lambda exchange: has_OHLCV_valuetype(exchange, False), ccxt.exchanges ))

# Número de exchanges que no ofrecen OHLCV
len(unavailable_list)
# Número de exchanges que ofrecesn OHLCV
len(ccxt.exchanges) - len(unavailable_list)

def bitso_balance(api):
    all_tickers = api.public_get_ticker()
    mxn_quoted_markets = { key: value for key, value in map(lambda entry: ((entry['book'].split('_')[0]).upper(), entry), filter(lambda ticker: 'mxn' in ticker['book'], all_tickers['payload']))}
    balance = api.fetch_balance()
    return reduce(lambda accum, market: accum + float(mxn_quoted_markets[market]['last']) * balance[market]['total'], mxn_quoted_markets, balance['MXN']['total'])

def bitso_detailed_balance(api):
    fetched_balance = api.fetch_balance()
    balances = pd.DataFrame(fetched_balance, columns=['used', 'free', 'total'])
    all_tickers = api.public_get_ticker()
    mxn_quoted_markets = { key: value for key, value in map(lambda entry: ((entry['book'].split('_')[0]).upper(), entry), filter(lambda ticker: 'mxn' in ticker['book'], all_tickers['payload']))}
    markets_df = pd.DataFrame(mxn_quoted_markets).transpose()
    balance_resume_df = pd.concat([markets_df[['ask', 'bid', 'last']].astype('float64'), balances], axis=1)
    balance_resume_df.at['MXN', 'last'] = 1.0
    balance_resume_df['balance_mxn'] = balance_resume_df['last'] * balance_resume_df['total']
    return balance_resume_df

# Market orders (api.create_market_sell_order, api.create_market_buy_order): symbol (major/minor), major_quantity, params
# Limit orders (api.create_limit_sell_order, api.create_limit_buy_order): symbol (major/minor), major_quantity, minor_quantity

# Ejemplos:
# ----------------------------------------
# In [34]: bitso_api.create_market_sell_order('XRP/MXN', 1)
# Out[34]:
# {'id': 'VyBlP0m4S9nLm0l8',
#  'info': {'payload': {'oid': 'VyBlP0m4S9nLm0l8'}, 'success': True}}
# ----------------------------------------
# In [40]: bitso_api.create_market_buy_order('XRP/MXN', 0.5)
# Out[40]:
# {'id': 'NVL4nE2sxMepmDH2',
#  'info': {'payload': {'oid': 'NVL4nE2sxMepmDH2'}, 'success': True}}


kraken_file = open(abspath(join(script_path, '../api_keys/kraken_secret.json'))).read()
kraken_json = json.loads(kraken_file)

kraken_api = ccxt.kraken({'apiKey': kraken_json['apiKey'], 'secret': kraken_json['secret']})
kraken_api.load_markets()

poloniex_file = open(abspath(join(script_path, '../api_keys/poloniex_secret.json'))).read()
poloniex_json = json.loads(poloniex_file)

poloniex_api = ccxt.poloniex({'apiKey': poloniex_json['apiKey'], 'secret': poloniex_json['secret']})
poloniex_api.load_markets()
