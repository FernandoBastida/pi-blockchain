##################
# Bitso API test #
##################
import time
import hmac
import hashlib
import requests
import json

secret_file = open('bitso_secret.json').read()
bitso_api = json.loads(secret_file)

# bitso_key = "BITSO_KEY"
# bitso_secret = "BITSO_SECRET"

http_method = "GET"
request_path = "/v3/balance/"
json_payload = ""

nonce =  str(int(round(time.time() * 1000)))

# Create signature
message = nonce+http_method+request_path+json_payload
signature = hmac.new(bitso_api['secreto'].encode('utf-8'),
                                            message.encode('utf-8'),
                                            hashlib.sha256).hexdigest()

# Build the auth header
auth_header = 'Bitso %s:%s:%s' % (bitso_api['clave'], nonce, signature)

# Send request
response = requests.get("https://api.bitso.com/v3/balance/", headers={"Authorization": auth_header})

print(response.content)
