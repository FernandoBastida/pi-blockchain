* Introducción

  Este vagrant box permite la instalación y desarrollo del algoritmo
  pi-blockchain

* Requerimientos

** =vagrant-scp=

   Se utiliza el plugin ~vagrant-scp~ para la sincronización de API
   keys:

*** Instalación

    #+BEGIN_SRC sh
      vagrant plugin install vagrant-scp
    #+END_SRC

*** Copiado de claves

    #+BEGIN_SRC sh
      # En la ruta relativa pi-blockchain/scripts/
      vagrant scp ../api_keys/ /home/vagrant/pi-blockchain
    #+END_SRC

*** Utilización

    Es suficiente con ejecutar =vagrant up=, el box instala las
    dependencias necesarias

#+OPTIONS: toc:nil
