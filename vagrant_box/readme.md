# Introducción

Este vagrant box permite la instalación y desarrollo del algoritmo
pi-blockchain

# Requerimientos

## `vagrant-scp`

Se utiliza el plugin `vagrant-scp` para la sincronización de API
keys:

### Instalación

    vagrant plugin install vagrant-scp

### Copiado de claves

    # En la ruta relativa pi-blockchain/scripts/
    vagrant scp ../api_keys/ /home/vagrant/pi-blockchain

### Utilización

Es suficiente con ejecutar `vagrant up`, el box instala las dependencias necesarias
